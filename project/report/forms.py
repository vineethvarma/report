from django import forms
from .models import AbcoPoDetails, IrsDetails, IrsInvoiceDetails, IrsInvoicePayment, Partner, PartnerAccount, PaymentDueDays,User,Funnel,SprDetails, SprInvoiceDetails, SprInvoicePayment,UserTeamMember


class PartnerForm(forms.ModelForm):
    
    class Meta:
        model=Partner
        fields = "__all__"


class PaymentDueDaysForm(forms.ModelForm):
    
    class Meta:
        model=PaymentDueDays
        fields = "__all__"


class UserForm(forms.ModelForm):
    
    class Meta:
        model=User
        fields = "__all__"


class AbcoPoDetailsForm(forms.ModelForm):
    
    class Meta:
        model=AbcoPoDetails
        fields = "__all__"

class IrsInvoiceDetailsForm(forms.ModelForm):
    
    class Meta:
        model=IrsInvoiceDetails
        fields = "__all__"

class IrsInvoicePaymentForm(forms.ModelForm):
    
    class Meta:
        model=IrsInvoicePayment
        fields = "__all__"
class IrsDetailsForm(forms.ModelForm):
    
    class Meta:
        model=IrsDetails
        fields = "__all__"
class PartnerAccountForm(forms.ModelForm):
    
    class Meta:
        model=PartnerAccount
        fields = "__all__"

class FunnelForm(forms.ModelForm):
    
    class Meta:
        model=Funnel
        fields = "__all__"


class SprInvoiceDetailsForm(forms.ModelForm):
    
    class Meta:
        model=SprInvoiceDetails
        fields = "__all__"

class SprInvoicePaymentForm(forms.ModelForm):
    
    class Meta:
        model=SprInvoicePayment
        fields = "__all__"
class SprDetailsForm(forms.ModelForm):
    
    class Meta:
        model=SprDetails
        fields = "__all__"