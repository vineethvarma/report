from django.db import models

# Create your models here.
class User(models.Model):
    user_id=models.IntegerField("User ID",primary_key=True)
    user_name = models.CharField("User Name",max_length=255)
    is_active = models.IntegerField("Is Active")
    user_role_id = models.IntegerField("User Role")
    solid_company_id = models.IntegerField("Solid Company")

    class Meta:
        db_table = "user"

class Partner(models.Model):
    user_id=models.IntegerField("User ID",primary_key=True)
    partner_id = models.IntegerField("Partner ID")

    class Meta:
        db_table = "partner"

class PartnerAccount(models.Model):
    user_id=models.IntegerField("User ID",primary_key=True)
    partner_account_id=models.IntegerField("Partner Account")
    partner_id=models.IntegerField("User ID")

    class Meta:
        db_table = "partner_account"

class Funnel(models.Model):
    funnel_quote_status_id=models.IntegerField("Funnel Status ID",primary_key=True)
    partner_account_id=models.IntegerField("Partner Account")
    funnel_id=models.IntegerField("Funnel ID")

    class Meta:
        db_table = "funnel"

class AbcoPoDetails(models.Model):
    funnel_id=models.IntegerField("Funnel ID")
    id=models.IntegerField("Abco Po",primary_key=True)
    # is_approved=models.IntegerField("Is Approved")
    class Meta:
        db_table = "abco_po_details"

class PaymentDueDays(models.Model):
    id=models.IntegerField("Abco Po",primary_key=True)
    days=models.IntegerField("Days")

    class Meta:
        db_table = "payment_due_days"

#########################################################################
class IrsDetails(models.Model):
    id=models.IntegerField("Abco Po",primary_key=True)
    abco_po_detail_id=models.IntegerField("Abco Po")
    payment_due_days_id=models.IntegerField("Payment Due Days")

    class Meta:
        db_table = "irs_details"



class IrsInvoiceDetails(models.Model):
    irs_details_id=models.IntegerField("Irs Details")
    id=models.IntegerField("Abco Po",primary_key=True)
    payment_due_date=models.DateField("Payment Due Date")
    invoice_due_date=models.DateField("Invoice Due Date")
    invoice_amount=models.IntegerField("Invoice Account")

    class Meta:
        db_table = "irs_invoice_details"

class IrsInvoicePayment(models.Model):
    payment_type_id=models.IntegerField("Payment Type")
    full_payment=models.IntegerField("Full Payment")
    partial_payment=models.IntegerField("Partial Payment")
    id=models.IntegerField("Abco Po",primary_key=True)
    irs_invoice_details_id=models.IntegerField("IRS Invoice Details")
    payment_date=models.DateField("Payment Date")
    created_at=models.DateField("Created At")

    class Meta:
        db_table = "irs_invoice_payment"

######################################################################

class SprDetails(models.Model):
    id=models.IntegerField("Abco Po",primary_key=True)
    abco_po_detail_id=models.IntegerField("Abco Po")
    payment_due_days_id=models.IntegerField("Payment Due Days")

    class Meta:
        db_table = "spr_details"



class SprInvoiceDetails(models.Model):
    spr_details_id=models.IntegerField("Irs Details")
    id=models.IntegerField("Abco Po",primary_key=True)
    payment_due_date=models.DateField("Payment Due Date")
    invoice_due_date=models.DateField("Invoice Due Date")
    #invoice_amount=models.IntegerField("Invoice Account")

    class Meta:
        db_table = "spr_invoice_details"

class SprInvoicePayment(models.Model):
    payment_type_id=models.IntegerField("Payment Type")
    full_payment=models.IntegerField("Full Payment")
    partial_payment=models.IntegerField("Partial Payment")
    id=models.IntegerField("Abco Po",primary_key=True)
    spr_invoice_details_id=models.IntegerField("IRS Invoice Details")

    class Meta:
        db_table = "spr_invoice_payment"

class UserTeamMember(models.Model):
    user_id=models.IntegerField("User ID")
    team_member_id=models.IntegerField("Team Member ID")

    class Meta:
        db_table = "user_team_member"