from django.urls import path
from .  import views

urlpatterns=[
    path('fetch<int:user_id><int:choice>',views.index_all,name='fetch'),
    path('stl',views.Stl,name='Stl'),
    path('',views.index,name='main'),
    
]