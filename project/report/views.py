import datetime
from django.shortcuts import render
import json
import numpy
import pandas as pd
from django.views.decorators.http import require_http_methods
from django.db import connection
from django.http import HttpResponse
import pandas as pd 
import datetime as dt
from .forms import PartnerForm
from .models import Partner,PartnerAccount,User,Funnel,AbcoPoDetails,IrsDetails,IrsInvoiceDetails,IrsInvoicePayment,PaymentDueDays,SprDetails,SprInvoiceDetails,SprInvoicePayment,UserTeamMember
from .forms import UserForm
from django.db.models import Q
from datetime import date




def Stl(request):
    data_user = User.objects.filter(Q(is_active=1)&(Q(user_role_id=2 )))
    Data_user=[]
    for i in data_user:
        data_user={
            i.user_id:'user_id',
        }
        Data_user.append(data_user)
    df1_user = pd.DataFrame([[ij for ij in i] for i in Data_user])
    df1_user.rename(columns={0: 'user_id_of_kam'}, inplace=True)

    data_user = User.objects.filter(Q(is_active=1)&(Q(user_role_id=1)))
    Data_user=[]
    for i in data_user:
        data_user={
            i.user_id:'user_id',
            
        }
        Data_user.append(data_user)
    df2 = pd.DataFrame([[ij for ij in i] for i in Data_user])
    df2.rename(columns={0: 'user_id_of_kam'}, inplace=True)
    data_user = UserTeamMember.objects.all()
    Data_user=[]
    for i in data_user:
        data_user={
            i.user_id:'user_id',
            i.team_member_id:'team_member_id',
            
        }
        Data_user.append(data_user)
    df_team = pd.DataFrame([[ij for ij in i] for i in Data_user])
    df_team.rename(columns={0: 'user_id_of_kam',1:'user_id'}, inplace=True)
    
    df_of_kam = pd.merge(df_team, df1_user)
    data_user = User.objects.filter(Q(is_active=1)&(Q(user_role_id=2 )))
    Data_user=[]
    for i in data_user:
        data_user={
            i.user_id:'user_id',
        }
        Data_user.append(data_user)
    df1 = pd.DataFrame([[ij for ij in i] for i in Data_user])
    df1.rename(columns={0: 'user_id_of_kam'}, inplace=True)
    df1['user_id_stl'] = df1['user_id_of_kam'] 
    df_of_kam= pd.merge(df_of_kam, df1,on="user_id_of_kam")
    common = df2.merge(df_of_kam,on=['user_id_of_kam'])
    df3=df2[(~df2.user_id_of_kam.isin(common.user_id_of_kam))]
#########################
    df3.rename(columns={'user_id_of_kam':'user_id'}, inplace=True)
    common = df3.merge(df_of_kam,on=['user_id'])
    df_independent=df3[(~df3.user_id.isin(common.user_id))]
    data_partner = Partner.objects.all()
    Data_partner=[]
    for i in data_partner:
        data_partner={
            i.user_id:'user_id',
            i.partner_id:'partner_id',
            
        }
        Data_partner.append(data_partner)
    df1_partner = pd.DataFrame([[ij for ij in i] for i in Data_partner])
    df1_partner.rename(columns={0: 'user_id',1:'partner_id'}, inplace=True)
    df_independent=pd.merge(df_independent, df1_partner)
    df = pd.merge(df_of_kam, df1_partner)
    data_partner_account = PartnerAccount.objects.all()
    Data_partner_account=[]
    for i in data_partner_account:
        data_partner_account={
            i.user_id:'user_id',
            i.partner_id:'partner_id',
            i.partner_account_id:'partner_account_id',
            
        }
        Data_partner_account.append(data_partner_account)
    df1_account = pd.DataFrame([[ij for ij in i] for i in Data_partner_account])
    df1_account.rename(columns={0: 'user_id',1:'partner_id',2:'partner_account_id'}, inplace=True)
    df = pd.merge(df, df1_account)
    df_independent = pd.merge(df_independent, df1_account)
    
    df.sort_values(by='partner_account_id', ascending=False).groupby(level=0).first()
    data_funnel = Funnel.objects.all()
    
    Data_funnel=[]
    for i in data_funnel:
        data_funnel={
            i.partner_account_id:'partner_account_id',
            i.funnel_quote_status_id:'funnel_quote_status_id',
            i.funnel_id:'funnel_id',
        }
        Data_funnel.append(data_funnel)
    df2_funnel = pd.DataFrame([[ij for ij in i] for i in Data_funnel])
    df2_funnel.rename(columns={ 0: 'partner_account_id',1: 'funnel_quote_status_id',2:'funnel_id'}, inplace=True)
    df2_funnel = df2_funnel.loc[df2_funnel['funnel_quote_status_id'] == 3, ["partner_account_id","funnel_quote_status_id","funnel_id"]]
    df = pd.merge(df, df2_funnel)
    df_independent = pd.merge(df_independent, df2_funnel)
    ########################################################################################
    data_abco = AbcoPoDetails.objects.all()
    
    Data_abco=[]
    for i in data_abco:
        data_abco={
            i.funnel_id:'funnel_id',
            i.id:'id',
        }
        Data_abco.append(data_abco)
    df2_abco = pd.DataFrame([[ij for ij in i] for i in Data_abco])
    df2_abco.rename(columns={ 0: 'funnel_id',1: 'abco_po_detail_id'}, inplace=True)
    
    df = pd.merge(df, df2_abco)
    df_independent = pd.merge(df_independent, df2_abco)
    ###########################################################################################
    data_irs = IrsDetails.objects.all()
    
    Data_irs=[]
    for i in data_irs:
        data_irs={
            i.abco_po_detail_id:'abco_po_detail_id',
            i.payment_due_days_id:'payment_due_days_id',
            i.id:'id'
        }
        Data_irs.append(data_irs)
    df2_irs_details = pd.DataFrame([[ij for ij in i] for i in Data_irs])
    df2_irs_details.rename(columns={ 0: 'abco_po_detail_id',1:'payment_due_days_id',2:'irs_details_id'}, inplace=True)
    
    df_irs = pd.merge(df, df2_irs_details)
    df_independent_irs = pd.merge(df_independent, df2_irs_details)
    #######################################################################################
    data_irs_invoice = IrsInvoiceDetails.objects.all()
    
    Data_irs_invoice=[]
    for i in data_irs_invoice:
        data_irs_invoice={
            i.invoice_due_date:'invoice_due_date',
            i.payment_due_date:'payment_due_date',
            i.irs_details_id:'irs_details_id',
            i.invoice_amount:'invoice_amount',
            i.id:'id'
        }
        Data_irs_invoice.append(data_irs_invoice)
    df2_irs_invoice = pd.DataFrame([[ij for ij in i] for i in Data_irs_invoice])
    df2_irs_invoice.rename(columns={ 0: 'invoice_due_date',1:'payment_due_date',2:'irs_details_id',3:'invoice_amount',4:'irs_invoice_details_id'}, inplace=True)
    df_irs = pd.merge(df_irs, df2_irs_invoice)
    df_independent_irs = pd.merge(df_independent_irs, df2_irs_invoice)
    df_irs=df_irs.drop(['partner_account_id','partner_id','funnel_id','funnel_quote_status_id'], axis = 1)
    df_independent_irs=df_independent_irs.drop(['partner_account_id','partner_id','funnel_id','funnel_quote_status_id'], axis = 1)
    df_irs['payment_date_change_invoice'] = pd.to_datetime(df_irs['payment_due_date'])
    my_date = datetime.date.today()
    year, week_num, day_of_week = my_date.isocalendar()
    df_independent_irs['payment_date_change_invoice'] = pd.to_datetime(df_independent_irs['payment_due_date'])
    my_date_in = datetime.date.today()
    todays_date = date.today()
    month = todays_date.month
    if ( month >= 4 ):
        start_year = year
        end_year = year + 1

    else:
        end_year = year
        start_year = year - 1

    
    d = datetime.datetime(start_year, 4, 1)
    date_start = d.date()
    d = datetime.datetime(end_year, 3, 31)
    date_end = d.date()
    total_out_invoice = df_irs.loc[(df_irs['payment_due_date'] >= date_start )& (df_irs['payment_due_date'] <= todays_date ), ["invoice_amount","user_id_of_kam",'payment_due_date','user_id','user_id_stl']]
    total_out_invoice_in = df_independent_irs.loc[(df_independent_irs['payment_due_date'] >= date_start )& (df_independent_irs['payment_due_date'] <= todays_date ), ["invoice_amount",'payment_due_date','user_id']]
    total_out_invoice_dataframe=total_out_invoice.groupby(['user_id_of_kam','user_id'])['invoice_amount'].sum().reset_index()
    l_out_invoice_total=[total_out_invoice_dataframe]
    total_out_invoice_dataframe=total_out_invoice.groupby(['user_id_of_kam','user_id_stl'])['invoice_amount'].sum().reset_index()
    l_out_invoice_total_stl=[total_out_invoice_dataframe]
    total_out_invoice_dataframe_in=total_out_invoice_in.groupby(['user_id'])['invoice_amount'].sum().reset_index()
    l_out_invoice_total_in=[total_out_invoice_dataframe_in]

    data_irs_invoice_pay = IrsInvoicePayment.objects.all()
    
    Data_irs_invoice_pay=[]
    for i in data_irs_invoice_pay:
        data_irs_invoice_pay={
            i.payment_type_id:'payment_type_id',
            i.full_payment:'full_payment',
            i.partial_payment:'partial_payment',
            i.irs_invoice_details_id:'irs_invoice_details_id',
            i.payment_date:'payment_date',
            i.created_at:'created_at',
        }
        Data_irs_invoice_pay.append(data_irs_invoice_pay)
    df2_irs_payment = pd.DataFrame([[ij for ij in i] for i in Data_irs_invoice_pay])
    df2_irs_payment.rename(columns={ 0: 'payment_type_id',1:'full_payment',2:'partial_payment',3:'irs_invoice_details_id',4:'payment_date',5:'created_at'}, inplace=True)
    df_irs = pd.merge(df_irs,df2_irs_payment,on='irs_invoice_details_id')
    df_irs['payment_date_change'] = pd.to_datetime(df_irs['created_at'])
    df_irs['created_at'] = pd.to_datetime(df_irs['created_at']).dt.date
    data_payment_day = PaymentDueDays.objects.all()
    
    Data_payment_day=[]
    for i in data_payment_day:
        data_payment_day={
            i.days:'days',
            i.id:'id'
        }
        Data_payment_day.append(data_payment_day)
    df2_irs_payday = pd.DataFrame([[ij for ij in i] for i in Data_payment_day])
    df2_irs_payday.rename(columns={ 0: 'days_overdue',1:'payment_due_days_id'}, inplace=True)
    df_irs = pd.merge(df_irs, df2_irs_payday)
    df_irs=df_irs.drop(['abco_po_detail_id','payment_due_days_id','invoice_amount','invoice_due_date','days_overdue','irs_details_id','invoice_due_date','payment_date_change_invoice'], axis = 1)
    df_irs['payment_due_date']=df_irs["payment_due_date"] - df_irs["payment_date"]
    df_irs=df_irs.rename(columns = {'payment_due_date':'overdue_days'})
    df_irs['overdue_days']=df_irs['overdue_days'].astype('timedelta64[D]')
    
    df_irs_payment = df_irs.loc[df_irs['overdue_days'] <= 0, ["full_payment",'user_id_of_kam','created_at','user_id']]
    total_out_invoice_y = df_irs_payment.loc[(df_irs_payment['created_at'] >= date_start )& (df_irs_payment['created_at'] <= todays_date ), ["full_payment","user_id_of_kam",'created_at','user_id']]
    total_out_invoice_date=total_out_invoice_y.groupby(['user_id_of_kam','user_id'])['full_payment'].sum().reset_index()
    l_total_collns_full=[total_out_invoice_date]

    df_irs_payment = df_irs.loc[df_irs['overdue_days'] <= 0, ["full_payment",'user_id_of_kam','created_at','user_id_stl']]
    total_out_invoice_y = df_irs_payment.loc[(df_irs_payment['created_at'] >= date_start )& (df_irs_payment['created_at'] <= todays_date ), ["full_payment","user_id_of_kam",'created_at','user_id_stl']]
    total_out_invoice_date=total_out_invoice_y.groupby(['user_id_of_kam','user_id_stl'])['full_payment'].sum().reset_index()
    l_total_collns_full_stl=[total_out_invoice_date]
    
    # ##########################################################
    df_irs_payment = df_irs.loc[df_irs['overdue_days'] > 0, ["full_payment",'user_id_of_kam','created_at','user_id']]
    total_out_invoice_y = df_irs_payment.loc[(df_irs_payment['created_at'] >= date_start )& (df_irs_payment['created_at'] <= todays_date ), ["full_payment","user_id_of_kam",'created_at','user_id']]
    total_out_invoice_date=total_out_invoice_y.groupby(['user_id_of_kam','user_id'])['full_payment'].sum().reset_index()
    l_total_over_collns_full=[total_out_invoice_date]
    
    df_irs_payment = df_irs.loc[df_irs['overdue_days'] > 0, ["full_payment",'user_id_of_kam','created_at','user_id_stl']]
    total_out_invoice_y = df_irs_payment.loc[(df_irs_payment['created_at'] >= date_start )& (df_irs_payment['created_at'] <= todays_date ), ["full_payment","user_id_of_kam",'created_at','user_id_stl']]
    total_out_invoice_date=total_out_invoice_y.groupby(['user_id_of_kam','user_id_stl'])['full_payment'].sum().reset_index()
    l_total_over_collns_full_stl=[total_out_invoice_date]
    
    ##########################################################
    
    # res_lt = [] # declaration of the list  
    # for x in range (0, len (l_total_collns_full)):  
    #     res_lt.append( l_total_collns_full[x] + l_total_over_collns_full[x])  
    # res_lt1 = []
    # for x in range (0, len (l_total_collns_full)):  
    #     res_lt1.append( l_out_invoice_total[x] - res_lt[x]) 
    #l=[[l_total_collns_full],[l_total_over_collns_full],[l_total_collns_full],[res_lt1]]
    l=l_out_invoice_total[0]
    output = pd.DataFrame(l)
    l_total_over_collns_full=pd.DataFrame(l_total_over_collns_full[0])
    l_total_collns_full=pd.DataFrame(l_total_collns_full[0])
    l_total_over_collns_full_stl=pd.DataFrame(l_total_over_collns_full_stl[0])
    l_total_collns_full_stl=pd.DataFrame(l_total_collns_full_stl[0])
    l_out_invoice_total_stl=pd.DataFrame(l_out_invoice_total_stl[0])
    
    l_out_invoice_total_in=pd.DataFrame(l_out_invoice_total_in[0])
    l_total_collns_full.rename(columns = {'full_payment':'collns'}, inplace = True)
    l_out_invoice_total_stl.rename(columns = {'invoice_amount':'invoice_amount_stl'}, inplace = True)
    l_total_collns_full_stl.rename(columns = {'full_payment':'collns_stl'}, inplace = True)
    l_out_invoice_total_in.rename(columns = {'user_id':'user_id_in'}, inplace = True)
    l_out_invoice_total_in.rename(columns = {'invoice_amount':'invoice_amount_in'}, inplace = True)
    output = pd.merge(output, l_total_over_collns_full,how = "outer")
    output = pd.merge(output, l_total_collns_full,how = "outer")
    output = pd.merge(output, l_total_over_collns_full_stl,how = "outer")
    output = pd.merge(output, l_total_collns_full_stl,how = "outer")
    output = pd.merge(output, l_out_invoice_total_stl,how = "outer")
    output=pd.concat([output, l_out_invoice_total_in], axis=1)
    print (output)
    data = User.objects.all()
    Data=[]
    for i in data:
        data={
            i.user_name:'user_name',
            i.user_id:'user_id',
        }
        Data.append(data)
    df2 = pd.DataFrame([[ij for ij in i] for i in Data])
    df2.rename(columns={ 0: 'user_name',1:'user_id_of_kam'}, inplace=True)
    output = pd.merge(output, df2)
    data = User.objects.all()
    Data=[]
    for i in data:
        data={
            i.user_name:'user_name',
            i.user_id:'user_id',
        }
        Data.append(data)
    df2 = pd.DataFrame([[ij for ij in i] for i in Data])
    df2.rename(columns={ 0: 'under_name',1:'user_id'}, inplace=True)
    
    output = pd.merge(output, df2)
    output_fromatted = output.to_json(orient='records')
    ################################################################
   
    return HttpResponse(output_fromatted)

###########################################################################################

def index_all(request,user_id,choice):
    if(choice==2):
        data_user = User.objects.filter(Q(is_active=1)&(Q(user_role_id=2 )| Q(user_role_id=1)))
        Data_user=[]
        for i in data_user:
            data_user={
                i.user_id:'user_id',
                
            }
            Data_user.append(data_user)
        df = pd.DataFrame([[ij for ij in i] for i in Data_user])
        df.rename(columns={0: 'user_id'}, inplace=True)
    if(choice==1):
        data_user = User.objects.filter(Q(is_active=1)& Q(solid_company_id=1)&(Q(user_role_id=2 )| Q(user_role_id=1)))
        Data_user=[]
        for i in data_user:
            data_user={
                i.user_id:'user_id',
                
            }
            Data_user.append(data_user)
        df = pd.DataFrame([[ij for ij in i] for i in Data_user])
        df.rename(columns={0: 'user_id'}, inplace=True)
    if(user_id>0):
        data_user = User.objects.filter(Q(is_active=1)& Q(solid_company_id=1)&(Q(user_role_id=2 )| Q(user_role_id=1)))
        Data_user=[]
        for i in data_user:
            data_user={
                i.user_id:'user_id',
                
            }
            Data_user.append(data_user)
        df = pd.DataFrame([[ij for ij in i] for i in Data_user])
        df.rename(columns={0: 'user_id'}, inplace=True)
        df = df.loc[df['user_id'] == user_id, ["user_id"]]
    
    ################################################################
    data_partner = Partner.objects.all()
    Data_partner=[]
    for i in data_partner:
        data_partner={
            i.user_id:'user_id',
            i.partner_id:'partner_id',
            
        }
        Data_partner.append(data_partner)
    df1_partner = pd.DataFrame([[ij for ij in i] for i in Data_partner])
    df1_partner.rename(columns={0: 'user_id',1:'partner_id'}, inplace=True)
    df = pd.merge(df, df1_partner)
    ################################################################
    data_partner_account = PartnerAccount.objects.all()
    Data_partner_account=[]
    for i in data_partner_account:
        data_partner_account={
            i.user_id:'user_id',
            i.partner_id:'partner_id',
            i.partner_account_id:'partner_account_id',
            
        }
        Data_partner_account.append(data_partner_account)
    df1_account = pd.DataFrame([[ij for ij in i] for i in Data_partner_account])
    df1_account.rename(columns={0: 'user_id',1:'partner_id',2:'partner_account_id'}, inplace=True)
    df = pd.merge(df, df1_account)
    
    df.sort_values(by='partner_account_id', ascending=False).groupby(level=0).first()

    #########################################################################################
    data_funnel = Funnel.objects.all()
    
    Data_funnel=[]
    for i in data_funnel:
        data_funnel={
            i.partner_account_id:'partner_account_id',
            i.funnel_quote_status_id:'funnel_quote_status_id',
            i.funnel_id:'funnel_id',
        }
        Data_funnel.append(data_funnel)
    df2_funnel = pd.DataFrame([[ij for ij in i] for i in Data_funnel])
    df2_funnel.rename(columns={ 0: 'partner_account_id',1: 'funnel_quote_status_id',2:'funnel_id'}, inplace=True)
    df2_funnel = df2_funnel.loc[df2_funnel['funnel_quote_status_id'] == 3, ["partner_account_id","funnel_quote_status_id","funnel_id"]]
    df = pd.merge(df, df2_funnel)
    ########################################################################################
    data_abco = AbcoPoDetails.objects.all()
    
    Data_abco=[]
    for i in data_abco:
        data_abco={
            i.funnel_id:'funnel_id',
            i.id:'id',
        }
        Data_abco.append(data_abco)
    df2_abco = pd.DataFrame([[ij for ij in i] for i in Data_abco])
    df2_abco.rename(columns={ 0: 'funnel_id',1: 'abco_po_detail_id'}, inplace=True)
    
    df = pd.merge(df, df2_abco)

    ###########################################################################################
    data_irs = IrsDetails.objects.all()
    
    Data_irs=[]
    for i in data_irs:
        data_irs={
            i.abco_po_detail_id:'abco_po_detail_id',
            i.payment_due_days_id:'payment_due_days_id',
            i.id:'id'
        }
        Data_irs.append(data_irs)
    df2_irs = pd.DataFrame([[ij for ij in i] for i in Data_irs])
    df2_irs.rename(columns={ 0: 'abco_po_detail_id',1:'payment_due_days_id',2:'irs_details_id'}, inplace=True)
    
    df_irs = pd.merge(df, df2_irs)
    
    #######################################################################################
    data_irs_invoice = IrsInvoiceDetails.objects.all()
    
    Data_irs_invoice=[]
    for i in data_irs_invoice:
        data_irs_invoice={
            i.invoice_due_date:'invoice_due_date',
            i.payment_due_date:'payment_due_date',
            i.irs_details_id:'irs_details_id',
            i.invoice_amount:'invoice_amount',
            i.id:'id'
        }
        Data_irs_invoice.append(data_irs_invoice)
    df2_irs = pd.DataFrame([[ij for ij in i] for i in Data_irs_invoice])
    df2_irs.rename(columns={ 0: 'invoice_due_date',1:'payment_due_date',2:'irs_details_id',3:'invoice_amount',4:'irs_invoice_details_id'}, inplace=True)
    df_irs = pd.merge(df_irs, df2_irs)
    df_irs=df_irs.drop(['partner_account_id','user_id','partner_id','funnel_id','funnel_quote_status_id'], axis = 1)
    df_irs['payment_date_change_invoice'] = pd.to_datetime(df_irs['payment_due_date'])
    df_irs['day_of_week_invoice'] = df_irs['payment_date_change_invoice'].dt.isocalendar().week
    my_date = datetime.date.today() # if date is 01/01/2018
    year, week_num, day_of_week = my_date.isocalendar()
    total_out_invoice = df_irs.loc[df_irs['day_of_week_invoice'] == week_num, ["invoice_amount"]]
    total_out_44=total_out_invoice['invoice_amount'].sum() 
    total_out_invoice = df_irs.loc[df_irs['day_of_week_invoice'] == week_num-1, ["invoice_amount"]]
    total_out_45=total_out_invoice['invoice_amount'].sum()
    total_out_invoice = df_irs.loc[df_irs['day_of_week_invoice'] == week_num-2, ["invoice_amount"]]
    total_out_46=total_out_invoice['invoice_amount'].sum()
    total_out_invoice = df_irs.loc[df_irs['day_of_week_invoice'] == week_num-3, ["invoice_amount"]]
    total_out_47=total_out_invoice['invoice_amount'].sum()
    l_total_out=[total_out_44,total_out_45,total_out_46,total_out_47]
    # ##################################################################################################
    data_irs_invoice_pay = IrsInvoicePayment.objects.all()
    
    Data_irs_invoice_pay=[]
    for i in data_irs_invoice_pay:
        data_irs_invoice_pay={
            i.payment_type_id:'payment_type_id',
            i.full_payment:'full_payment',
            i.partial_payment:'partial_payment',
            i.irs_invoice_details_id:'irs_invoice_details_id',
            i.payment_date:'payment_date',
            i.created_at:'created_at',
        }
        Data_irs_invoice_pay.append(data_irs_invoice_pay)
    df2_irs = pd.DataFrame([[ij for ij in i] for i in Data_irs_invoice_pay])
    df2_irs.rename(columns={ 0: 'payment_type_id',1:'full_payment',2:'partial_payment',3:'irs_invoice_details_id',4:'payment_date',5:'created_at'}, inplace=True)
    df_irs = pd.merge(df_irs, df2_irs)
    df_irs['payment_date_change'] = pd.to_datetime(df_irs['created_at'])
    df_irs['day_of_week'] = df_irs['payment_date_change'].dt.isocalendar().week
##################################################################################################################
    data_payment_day = PaymentDueDays.objects.all()
    
    Data_payment_day=[]
    for i in data_payment_day:
        data_payment_day={
            i.days:'days',
            i.id:'id'
        }
        Data_payment_day.append(data_payment_day)
    df2_irs = pd.DataFrame([[ij for ij in i] for i in Data_payment_day])
    df2_irs.rename(columns={ 0: 'days_overdue',1:'payment_due_days_id'}, inplace=True)
    df_irs = pd.merge(df_irs, df2_irs)
    df_irs=df_irs.drop(['abco_po_detail_id','payment_due_days_id','created_at','payment_date_change','days_overdue','irs_details_id','invoice_due_date','payment_date_change_invoice'], axis = 1)
    df_irs['payment_due_date']=df_irs["payment_date"] - df_irs["payment_due_date"]
    df_irs=df_irs.rename(columns = {'payment_due_date':'overdue_days'})
    df_irs['overdue_days']=df_irs['overdue_days'].astype('timedelta64[D]')
    df_irs.loc[(df_irs.overdue_days < 0), 'overdue_days'] = 0
    # df_irs_partial = df_irs.loc[df_irs['payment_type_id'] == 2, ["irs_invoice_details_id","partial_payment"]]
    # partial_payment= df_irs_partial.groupby(by='irs_invoice_details_id').sum()['partial_payment']
    # partial_payment.columns = ['irs_invoice_details_id','partial_payment']
    df_irs_payment = df_irs.loc[df_irs['overdue_days'] == 0, ["full_payment",'day_of_week']]
    ##########################################################################################################
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num, ["full_payment"]]
    total_collns_44=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-1, ["full_payment"]]
    total_collns_45=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-2, ["full_payment"]]
    total_collns_46=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-3, ["full_payment"]]
    total_collns_47=total_out_invoice['full_payment'].sum()
    l_total_collns=[total_collns_44,total_collns_45,total_collns_46,total_collns_47]

    #########################################################################################################
    df_irs_payment = df_irs.loc[df_irs['overdue_days'] > 0, ["full_payment",'day_of_week']]
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num, ["full_payment"]]
    total_over_collns_44=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-1, ["full_payment"]]
    total_over_collns_45=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-2, ["full_payment"]]
    total_over_collns_46=total_out_invoice['full_payment'].sum()
    total_out_invoice = df_irs_payment.loc[df_irs_payment['day_of_week'] == week_num-3, ["full_payment"]]
    total_over_collns_47=total_out_invoice['full_payment'].sum()
    l_total_over_collns=[total_over_collns_44,total_over_collns_45,total_over_collns_46,total_over_collns_47]
    res_lt = [] # declaration of the list  
    for x in range (0, len (l_total_collns)):  
        res_lt.append( l_total_collns[x] + l_total_over_collns[x])  
    res_lt1 = []
    for x in range (0, len (l_total_collns)):  
        res_lt1.append( l_total_out[x] - res_lt[x]) 
    #print(overdue_collns)
    l=[[l_total_collns],[l_total_over_collns],[l_total_out],[res_lt1]]
    output = pd.DataFrame(l).transpose()
    output.columns=['total_collns','overdue_collns','total_out','overdue']
    output_fromatted = output.to_json(orient='records')
    return HttpResponse(output_fromatted)
    ###########################################################################################
    # data_spr = SprDetails.objects.all()
    
    # Data_spr=[]
    # for i in data_spr:
    #     data_spr={
    #         i.abco_po_detail_id:'abco_po_detail_id',
    #         i.payment_due_days_id:'payment_due_days_id',
    #         i.id:'id'
    #     }
    #     Data_spr.append(data_spr)
    # df2_spr = pd.DataFrame([[ij for ij in i] for i in Data_spr])
    # df2_spr.rename(columns={ 0: 'abco_po_detail_id',1:'payment_due_days_id',2:'spr_details_id'}, inplace=True)
    
    # df_spr = pd.merge(df, df2_spr)
    
    # #######################################################################################
    # data_spr_invoice = SprInvoiceDetails.objects.all()
    
    # Data_spr_invoice=[]
    # for i in data_spr_invoice:
    #     data_spr_invoice={
    #         i.invoice_due_date:'invoice_due_date',
    #         i.payment_due_date:'payment_due_date',
    #         i.spr_details_id:'spr_details_id',
    #         #i.invoice_amount:'invoice_amount',
    #         i.id:'id'
    #     }
    #     Data_spr_invoice.append(data_spr_invoice)
    # df2_spr = pd.DataFrame([[ij for ij in i] for i in Data_spr_invoice])
    # df2_spr.rename(columns={ 0: 'invoice_due_date',1:'payment_due_date',2:'spr_details_id',3:'spr_invoice_details_id'}, inplace=True)
    # df_spr = pd.merge(df_spr, df2_spr)
    # df_spr=df_spr.drop(['partner_account_id','user_id','partner_id','funnel_id','funnel_quote_status_id'], axis = 1)
    
    # # ##################################################################################################
    # data_payment_day = PaymentDueDays.objects.all()
    
    # Data_payment_day=[]
    # for i in data_payment_day:
    #     data_payment_day={
    #         i.days:'days',
    #         i.id:'id'
    #     }
    #     Data_payment_day.append(data_payment_day)
    # df2_spr = pd.DataFrame([[ij for ij in i] for i in Data_payment_day])
    # df2_spr.rename(columns={ 0: 'days',1:'payment_due_days_id'}, inplace=True)
    # df_spr = pd.merge(df_spr, df2_spr)
    # df_spr=df_spr.drop(['abco_po_detail_id','payment_due_days_id'], axis = 1)
    # current_date = date.today()
    # df_spr.loc[:, "payment_due_date"] = df_spr["payment_due_date"].apply(lambda x: current_date-x)
    # df_spr=df_spr.rename(columns = {'payment_due_date':'overdue_days'})
    # print(df_spr)
    # ###################################################################################################
    # data_spr_invoice_pay = SprInvoicePayment.objects.all()
    
    # Data_spr_invoice_pay=[]
    # for i in data_spr_invoice_pay:
    #     data_spr_invoice_pay={
    #         i.payment_type_id:'payment_type_id',
    #         i.full_payment:'full_payment',
    #         i.partial_payment:'partial_payment',
    #         i.spr_invoice_details_id:'spr_invoice_details_id'
    #     }
    #     Data_spr_invoice_pay.append(data_spr_invoice_pay)
    # df2_spr = pd.DataFrame([[ij for ij in i] for i in Data_spr_invoice_pay])
    # df2_spr.rename(columns={ 0: 'payment_type_id',1:'full_payment',2:'partial_payment',3:'spr_invoice_details_id'}, inplace=True)
    # #df_spr = pd.merge(df_spr, df2_spr)
    # print(df_spr)
    

def index(request):
    data = User.objects.filter(Q(is_active=1)& (Q(user_role_id=2 )| Q(user_role_id=1)))
    Data=[]
    for i in data:
        data={
            i.user_name:'user_name',
            i.user_id:'user_id',
        }
        Data.append(data)
    return render(request,"index.html",{"data": Data})






